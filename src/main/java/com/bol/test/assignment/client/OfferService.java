package com.bol.test.assignment.client;

import com.bol.test.assignment.model.offer.Offer;

public interface OfferService {
    Offer getOffer(int id);
}
