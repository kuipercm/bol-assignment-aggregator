package com.bol.test.assignment.client;

import com.bol.test.assignment.model.product.Product;

public interface ProductService {
    Product getProduct(int id);
}
