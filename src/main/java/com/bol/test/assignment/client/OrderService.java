package com.bol.test.assignment.client;

import com.bol.test.assignment.model.order.Order;

public interface OrderService {
    Order getOrder(int sellerId);
}
