package com.bol.test.assignment.clientimpl.order;

import org.springframework.stereotype.Service;

import com.bol.test.assignment.client.OrderService;
import com.bol.test.assignment.model.order.Order;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRestClient restClient;

    @Override
    public Order getOrder(int sellerId) {
        return restClient.getOrderBySellerId(sellerId);
    }

}
