package com.bol.test.assignment.clientimpl.offer;

import static com.bol.test.assignment.model.offer.OfferCondition.AS_NEW;

import org.springframework.stereotype.Service;

import com.bol.test.assignment.client.OfferService;
import com.bol.test.assignment.model.offer.Offer;

@Service
public class DefaultOfferService implements OfferService {
    @Override
    public Offer getOffer(int id) {
        return new Offer(id, AS_NEW);
    }
}
