package com.bol.test.assignment.clientimpl.product;

import org.springframework.stereotype.Service;

import com.bol.test.assignment.client.ProductService;
import com.bol.test.assignment.model.product.Product;

@Service
public class DefaultProductService implements ProductService {
    @Override
    public Product getProduct(int id) {
        return new Product(id, "New Product");
    }
}
