package com.bol.test.assignment.clientimpl.order;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bol.test.assignment.model.order.Order;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class OrderRestClient {
    public static final String ORDER_BY_SELLER_CIRCUIT_BREAKER = "getOrderBySellerId";

    private static final String GET_ORDER_PATH_SUFFIX = "/order/{orderId}";

    private final RestTemplate restTemplate;

    public OrderRestClient(@Qualifier("orderRestTemplate") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @HystrixCommand(groupKey = "OrderGroup", commandKey = ORDER_BY_SELLER_CIRCUIT_BREAKER)
    public Order getOrderBySellerId(int sellerId) {
        return restTemplate.getForObject(GET_ORDER_PATH_SUFFIX, Order.class, sellerId);
    }

}
