package com.bol.test.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableHystrix
@EnableAspectJAutoProxy
public class AggregatorServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(AggregatorServiceApplication.class, args);
    }
}
