package com.bol.test.assignment.model.offer;

import static com.bol.test.assignment.model.offer.OfferCondition.UNKNOWN;

import lombok.Value;

@Value
public class Offer {
    public static final Offer UNKNOWN_OFFER = new Offer(-1, UNKNOWN);

    private final int id;
    private final OfferCondition condition;
}
