package com.bol.test.assignment.model.offer;

public enum OfferCondition {
    NEW,
    AS_NEW,
    OLD,
    UNKNOWN
}
