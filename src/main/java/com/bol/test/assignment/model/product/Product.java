package com.bol.test.assignment.model.product;

import lombok.Value;

@Value
public class Product {
    public static final Product UNKNOWN_PRODUCT = new Product(-1, null);

    private final int id;
    private final String title;
}
