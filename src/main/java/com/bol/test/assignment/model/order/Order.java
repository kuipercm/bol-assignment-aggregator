package com.bol.test.assignment.model.order;

import lombok.Value;

@Value
public class Order {
    private final int id;
    private final int offerId;
    private final int productId;
}
