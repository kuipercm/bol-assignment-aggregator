package com.bol.test.assignment.aggregator;

import static com.bol.test.assignment.model.offer.Offer.UNKNOWN_OFFER;
import static com.bol.test.assignment.model.product.Product.UNKNOWN_PRODUCT;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.bol.test.assignment.client.OfferService;
import com.bol.test.assignment.client.OrderService;
import com.bol.test.assignment.client.ProductService;
import com.bol.test.assignment.exception.EnrichingOrderException;
import com.bol.test.assignment.model.offer.Offer;
import com.bol.test.assignment.model.order.Order;
import com.bol.test.assignment.model.product.Product;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class EnrichedOrderService {
    private OrderService orderService;
    private OfferService offerService;
    private ProductService productService;

    public EnrichedOrder enrich(int sellerId) {
        log.debug("Enriching order for sellerid {}", sellerId);

        CompletableFuture<EnrichedOrder> futureOrder = getOrder(sellerId)
                .thenCompose(order -> {
                    CompletableFuture<Offer> futureOffer = getOffer(order.getOfferId());
                    CompletableFuture<Product> futureProduct = getProduct(order.getProductId());

                    CompletableFuture.allOf(futureOffer, futureProduct);

                    return CompletableFuture.supplyAsync(() -> {
                        try {
                            return combine(order, futureOffer.get(), futureProduct.get());
                        } catch (ExecutionException | InterruptedException e) {
                            log.error("Unknown error during retrieving either offer or product:", e);
                            throw new EnrichingOrderException(e);
                        }
                    });
                });
        try {
            return futureOrder.get();
        } catch (ExecutionException | InterruptedException e) {
            log.error("Retrieving order did not succeed:", e);
            throw new EnrichingOrderException(e);
        }
    }

    private EnrichedOrder combine(Order order, Offer offer, Product product) {
        return new EnrichedOrder(order.getId(), offer.getId(), offer.getCondition(), product.getId(), product.getTitle());
    }

    private CompletableFuture<Order> getOrder(int sellerId) {
        log.debug("Retrieving order for sellerId {}", sellerId);
        return CompletableFuture.supplyAsync(() -> orderService.getOrder(sellerId));
    }

    private CompletableFuture<Offer> getOffer(int offerId) {
        log.debug("Retrieving offer for offerId {}", offerId);
        return CompletableFuture.supplyAsync(() -> offerService.getOffer(offerId)).exceptionally(e -> UNKNOWN_OFFER);
    }

    private CompletableFuture<Product> getProduct(int productId) {
        log.debug("Retrieving product for productId {}", productId);
        return CompletableFuture.supplyAsync(() -> productService.getProduct(productId)).exceptionally(e -> UNKNOWN_PRODUCT);
    }
}
