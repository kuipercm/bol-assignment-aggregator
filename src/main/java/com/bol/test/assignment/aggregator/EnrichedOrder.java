package com.bol.test.assignment.aggregator;

import com.bol.test.assignment.model.offer.OfferCondition;

import lombok.Value;

@Value
public class EnrichedOrder {
    private final int id;
    private final int offerId;
    private final OfferCondition offerCondition;
    private final int productId;
    private final String productTitle;
}
