package com.bol.test.assignment.exception;

public class EnrichingOrderException extends RuntimeException {
    public EnrichingOrderException(Throwable e) {
        super(e);
    }
}
