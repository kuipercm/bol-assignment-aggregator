package com.bol.test.assignment.model.product;

import static com.bol.test.assignment.model.product.Product.UNKNOWN_PRODUCT;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class ProductTest {

    @Test
    public void verify_that_unknown_product_has_no_valid_id_and_null_title() {
        assertThat(UNKNOWN_PRODUCT.getId()).isEqualTo(-1);
        assertThat(UNKNOWN_PRODUCT.getTitle()).isNull();
    }

}