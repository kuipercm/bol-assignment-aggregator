package com.bol.test.assignment.model.offer;

import static com.bol.test.assignment.model.offer.Offer.UNKNOWN_OFFER;
import static com.bol.test.assignment.model.offer.OfferCondition.UNKNOWN;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class OfferTest {

    @Test
    public void verify_that_unknown_offer_has_no_valid_id_and_condition_unknown() {
        assertThat(UNKNOWN_OFFER.getId()).isEqualTo(-1);
        assertThat(UNKNOWN_OFFER.getCondition()).isEqualTo(UNKNOWN);
    }

}