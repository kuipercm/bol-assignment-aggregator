package com.bol.test.assignment.clientimpl.order;

import static com.bol.test.assignment.TestHelper.nextValue;
import static com.bol.test.assignment.clientimpl.order.OrderRestClient.ORDER_BY_SELLER_CIRCUIT_BREAKER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.AbstractConfiguration;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.cloud.netflix.archaius.ArchaiusAutoConfiguration;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.bol.test.assignment.model.order.Order;
import com.netflix.config.ConfigurationManager;
import com.netflix.hystrix.Hystrix;
import com.netflix.hystrix.exception.HystrixRuntimeException;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OrderRestClientHystrixIT.TestConfiguration.class, initializers = ConfigFileApplicationContextInitializer.class)
public class OrderRestClientHystrixIT {
    private static final int SELLER_ID1 = nextValue++;
    private static final int TIMEOUT_IN_MS = 500;
    private static final int TIMEOUT_DELTA = 100;

    @Autowired
    private RestTemplate orderRestTemplate;

    @Autowired
    private OrderRestClient sut;

    @After
    public void after() {
        Hystrix.reset();
    }

    @Test
    public void verify_that_hystrix_properties_are_set_correctly() {
        // If this test fails, there is probably something wrong with the test setup and all other tests will probably fail as well.
        AbstractConfiguration configuration = ConfigurationManager.getConfigInstance();
        long timeoutInMillis = configuration.getLong("hystrix.command." + ORDER_BY_SELLER_CIRCUIT_BREAKER + ".execution.isolation.thread.timeoutInMilliseconds");
        long requestThreshold = configuration.getLong("hystrix.command." + ORDER_BY_SELLER_CIRCUIT_BREAKER + ".circuitBreaker.requestVolumeThreshold");

        assertThat(timeoutInMillis).isEqualTo(TIMEOUT_IN_MS);
        assertThat(requestThreshold).isEqualTo(1);
    }

    @Test
    public void when_response_takes_shorter_than_the_timeout_then_no_exception_is_thrown() {
        when(orderRestTemplate.getForObject("/order/{orderId}", Order.class, SELLER_ID1)).thenAnswer(delayedAnswer(TIMEOUT_IN_MS - TIMEOUT_DELTA));

        sut.getOrderBySellerId(SELLER_ID1);
    }

    @Test(expected = HystrixRuntimeException.class)
    public void when_response_takes_longer_than_the_timeout_then_an_exception_is_thrown() {
        when(orderRestTemplate.getForObject("/order/{orderId}", Order.class, SELLER_ID1)).thenAnswer(delayedAnswer(TIMEOUT_IN_MS + TIMEOUT_DELTA));

        sut.getOrderBySellerId(SELLER_ID1);
    }

    private static Answer<Order> delayedAnswer(int delayInMillis) {
        return invocationOnMock -> {
            Thread.sleep(delayInMillis);
            return new Order(SELLER_ID1, 1, 2);
        };
    }

    @Configuration
    @Import({ArchaiusAutoConfiguration.class, OrderRestClient.class})
    @EnableHystrix
    @EnableAspectJAutoProxy(proxyTargetClass = true)
    @PropertySource("classpath:hystrix-test.properties")
    static class TestConfiguration {

        @Bean
        RestTemplate orderRestTemplate() {
            return mock(RestTemplate.class);
        }
    }

}