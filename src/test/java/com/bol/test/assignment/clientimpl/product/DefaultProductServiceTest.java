package com.bol.test.assignment.clientimpl.product;

import static com.bol.test.assignment.TestHelper.nextValue;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.bol.test.assignment.client.ProductService;
import com.bol.test.assignment.model.product.Product;

public class DefaultProductServiceTest {
    private static final int PRODUCT_ID1 = nextValue++;

    private final ProductService sut = new DefaultProductService();

    @Test
    public void test_default_implementation() {
        Product product = sut.getProduct(PRODUCT_ID1);

        assertThat(product).isNotNull();
        assertThat(product.getId()).isEqualTo(PRODUCT_ID1);
        assertThat(product.getTitle()).isEqualTo("New Product");
    }

}