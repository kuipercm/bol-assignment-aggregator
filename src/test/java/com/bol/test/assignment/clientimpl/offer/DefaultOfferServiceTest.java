package com.bol.test.assignment.clientimpl.offer;

import static com.bol.test.assignment.TestHelper.nextValue;
import static com.bol.test.assignment.model.offer.OfferCondition.AS_NEW;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.bol.test.assignment.client.OfferService;
import com.bol.test.assignment.model.offer.Offer;

public class DefaultOfferServiceTest {
    private static final int OFFER_ID1 = nextValue++;

    private final OfferService sut = new DefaultOfferService();

    @Test
    public void test_default_implementation() {
        Offer offer = sut.getOffer(OFFER_ID1);

        assertThat(offer).isNotNull();
        assertThat(offer.getId()).isEqualTo(OFFER_ID1);
        assertThat(offer.getCondition()).isEqualTo(AS_NEW);
    }

}