package com.bol.test.assignment.clientimpl.order;

import static com.bol.test.assignment.TestHelper.nextValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

public class OrderServiceImplTest {
    private static final int ORDER_ID1 = nextValue++;

    private OrderRestClient restClient;
    private OrderServiceImpl sut;

    @Before
    public void setup() {
        restClient = mock(OrderRestClient.class);
        sut = new OrderServiceImpl(restClient);
    }

    @Test
    public void when_getting_an_order_the_order_is_obtained_from_the_restclient() {
        sut.getOrder(ORDER_ID1);

        verify(restClient).getOrderBySellerId(ORDER_ID1);
    }

}