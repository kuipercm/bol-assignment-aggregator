package com.bol.test.assignment.clientimpl.order;

import static com.bol.test.assignment.TestHelper.nextValue;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.bol.test.assignment.model.order.Order;

public class OrderRestClientIT {
    private static final int SELLER_ID1 = nextValue++;

    private OrderRestClient sut;
    private MockRestServiceServer server;

    @Before
    public void setup() {
        RestTemplate restTemplate = new RestTemplate();
        server = MockRestServiceServer.createServer(restTemplate);

        sut = new OrderRestClient(restTemplate);
    }

    @Test
    public void verify_that_getting_order_results_in_rest_call_to_remote_server_with_correct_seller_id() {
        server.expect(requestTo("/order/" + SELLER_ID1))
                .andExpect(method(GET))
                .andRespond(withSuccess()
                        .body("{\"id\": " + SELLER_ID1 + ", \"offerId\": 1, \"productId\": 2}")
                        .contentType(APPLICATION_JSON_UTF8));

        Order order = sut.getOrderBySellerId(SELLER_ID1);

        assertThat(order.getId()).isEqualTo(SELLER_ID1);
        assertThat(order.getOfferId()).isEqualTo(1);
        assertThat(order.getProductId()).isEqualTo(2);
    }

    @Test(expected = HttpClientErrorException.class)
    public void verify_that_getting_order_where_remote_responds_with_client_error_results_in_exception() {
        server.expect(requestTo("/order/" + SELLER_ID1))
                .andExpect(method(GET))
                .andRespond(withStatus(NOT_FOUND));

        sut.getOrderBySellerId(SELLER_ID1);
    }

}